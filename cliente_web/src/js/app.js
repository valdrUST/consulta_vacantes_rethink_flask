$(document).ready(function () {
    namespace = '/consulta';
    var socket = io.connect(namespace);
    socket.on('connect', function() {
        socket.emit('my_event', {data: 'I\'m connected!'});
    });
    
    socket.on('kriko', function(msg, cb){
        console.log(msg);
    });

    socket.on('consulta_respuesta', function(msg, cb) {
        console.log(msg);
        console.log(cb);
    $('#log').text('Received #: ' + msg.data).html();
    if (cb)
        cb();
    });
    /*$('#dataTable').DataTable( {
        data: dataSet,
        columns: [
            { title: "Name" },
            { title: "Position" },
            { title: "Office" },
            { title: "Extn." },
            { title: "Start date" },
            { title: "Salary" }
        ]
    });*/
    var sockets = [];
    function update_data(msg, cb) {
        $("#p-"+msg.data.id_grupo_asignatura).text(msg.data.vacantes);
    }
    
    $(".card-body").on("click",".btn-liberar",function(){
        console.log("liberar");
        id_grupo_asig = $(this).attr("id").substr(4);
        res  = ajax_request("/ws/vacantes_app/liberar_vacante/"+id_grupo_asig, "post","");
    });
    
    $(".card-body").on("click",".btn-ocupar",function(){
        console.log("ocupar");
        id_grupo_asig = $(this).attr("id").substr(4);
        res  = ajax_request("/ws/vacantes_app/ocupar_vacante/"+id_grupo_asig, "post","");
    });

    $(".card-body").on("click","#submit-id-materia",function(){
        var id_materia = $("#id-materia-txt").val();
        res = ajax_request("/ws/vacantes_app/get_vacante/"+id_materia,"get","");
        data = res.data;
        var dataSet = [];
        sockets = [];
        data.forEach(grupo => {
            if(grupo.id_grupo_asignatura.substr(0,5) == "20211"){
                var btn = document.createElement("button");
                btn.setAttribute("id","btn-"+grupo.id_grupo_asignatura);
                btn.setAttribute("class","btn btn-primary btn-ocupar");
                btn.innerHTML = "ocupar";
                var btn2 = document.createElement("button");
                btn2.setAttribute("class","btn btn-primary btn-liberar");
                btn2.setAttribute("id","btn-"+grupo.id_grupo_asignatura);
                var vacante = document.createElement("p");
                vacante.setAttribute("id","p-"+grupo.id_grupo_asignatura);
                btn2.innerHTML = "liberar";
                sockets.push(socket.on('vacante_'+grupo.id_grupo_asignatura, update_data));
                vacante.innerHTML = grupo.vacantes;
                dataSet.push([grupo.id_asignatura,grupo.asignatura,grupo.numero,vacante.outerHTML,grupo.cupo,btn.outerHTML+btn2.outerHTML]);
            }
        });
        table = $('#dataTable').DataTable( {
            paging: true,
            data: dataSet
        });
        console.log(sockets);
        table.destroy();
    });


});
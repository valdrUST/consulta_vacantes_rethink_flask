#!/bin/python
from rethinkdb import RethinkDB
class Database(object):
    def __init__(self):
        self.r = RethinkDB()
        self.rconn = self.r.connect( "rethinkdb", 28015)
    
    def consulta(self,query):
        lista = []
        print(query)
        q = query.popitem()
        col = q[0]
        val = q[1]
        print(self.r.db("grupos").table("vacantes"))
        cursor = self.r.db("grupos").table("vacantes").filter(self.r.row[col] == val).run(self.rconn)
        for x in cursor:
            lista.append(x)
        return lista
    
    def update(self,query,newVal):
        q = query.popitem()
        col = q[0]
        val = q[1]
        q = newVal.popitem()
        newCol = q[0]
        newVal = q[1]
        return self.r.db("grupos").table("vacantes").filter(self.r.row[col] == val).update({newCol:newVal}).run(self.rconn)

# vacantes_app
## Instrucciones de uso
* para correr el servidor usando el WSGI
```bash
$ vacantes_appWSGI
```
* para correr en modo consola para desarrollo y debug
```bash
$ vacantes_app --ws --port 8001 --host 0.0.0.0
```

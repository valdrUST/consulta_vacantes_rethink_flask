from rethinkdb import RethinkDB
import json

r = RethinkDB()
rconn = r.connect( "rethinkdb", 28015)
try:
    r.db_create("grupos").run(rconn)
    r.db("grupos").table_create("vacantes").run(rconn)
except:
    print("tabla y base ya existentes")


def poblar_rethink():
    grupo_think = []
    with open("consulta.json","r") as f:
        grupos = f.read()
        gruposJ = json.loads(grupos)
    for grupo in gruposJ:
        grupo["id"] = grupo["id_grupo_asignatura"]
        grupo_think.append(grupo)
    for grupo in grupo_think:
        try:
            x = r.db("grupos").table("vacantes").insert(grupo).run(rconn)
        except Exception as e:
            pass
    consultar_think()

def consultar_think():
    res = []
    for x in r.db("grupos").table("vacantes").run(rconn):
        res.append(x)
    print(len(res))

if __name__ == "__main__":
    poblar_rethink()